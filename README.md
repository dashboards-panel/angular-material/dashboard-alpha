# Project with objective of demonstration to jobs

this project already has an admin CRUD component as seen in the print image 

## [Demo](https://)

[![Build Status](https://img.shields.io/travis/changhuixu/angular-material-admin/master.svg?label=Travis%20CI&style=flat-square)](https://app.travis-ci.com/changhuixu/angular-material-admin.svg?branch=main)

![Dashboard Screenshot](./img/dashboard.png)

## License

Feel free to use the code in this repository as it is under MIT license.

