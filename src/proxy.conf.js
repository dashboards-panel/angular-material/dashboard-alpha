
const PROXY_CONFIG = [
    {
        context: ['/api'],
        target: 'https://politicdash-api.onrender.com/',
        secure: false,
        logLevel: 'debug',
        changeOrigin: true,
        pathRewite: {
            '^/api': ''
        }
    }
];

module.exports = PROXY_CONFIG;