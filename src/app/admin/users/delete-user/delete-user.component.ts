import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {

  clicked: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: User,
    private service: UserService,
  public dialogRef: MatDialogRef<DeleteUserComponent>) { }

  ngOnInit(): void {
  }

  deleteUser() {
    this.service.deleteUser(this.data.id)
      .subscribe((result) => {
        this.closeModal(result);
      });
  }

  closeModal(event: any = null) {
    this.dialogRef.close(event);
  }

}
