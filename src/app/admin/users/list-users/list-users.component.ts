import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/User';
import { CreateUserComponent } from '../create-user/create-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { DetailsUserComponent } from '../details-user/details-user.component';
import { UserService } from '../user.service';


@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'email', 'createdAt', 'symbol'];
  dataSource = new MatTableDataSource<any>();
  form: FormGroup;

  constructor(
    private service: UserService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.service.getUsers()
      .subscribe((results) => {
          this.dataSource = new MatTableDataSource(results['administrators']);
      });
  }

  openModalForm(user: User = null) {
    const dialogRef = this.dialog.open(CreateUserComponent, {
      height: '430px',
      width: '650px',
      data: { user: user },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed ==> ', result);
      if(result !== null) {
        this.getUsers();
        this.message(result['message']);
      }
    });
  }
  
  openModalDetails(user: User = null) {
    const dialogRef = this.dialog.open(DetailsUserComponent, {
      height: '430px',
      width: '650px',
      data: user,
      disableClose: true
    });
  }
  
  openModalDelete(user: User = null) {
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      height: '430px',
      width: '650px',
      data: { id: user.id },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed ==> ', result);
      if(result !== null) {
        this.getUsers();
        this.message(result['message']);
      }
    });
  }

  message(message: string = '') {
    this._snackBar.open(message, 'close');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
