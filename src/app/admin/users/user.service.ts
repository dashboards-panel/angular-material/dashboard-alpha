import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { User } from 'src/app/models/User';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  public api: string = environment.api + '/dashboard/administrator'

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.api)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  createUser(user: any): Observable<User> {
    return this.http.post<User>(this.api, user)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  updateUser(user: any): Observable<User> {
    return this.http.put(this.api + `/${user.id}`, user).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  deleteUser(id: any): Observable<User> {
    return this.http.delete(this.api + `/${id}`)
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

}
