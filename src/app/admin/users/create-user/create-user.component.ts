import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  form: FormGroup;
  clicked: boolean = false;
  user: User;

  constructor(
    private fb: FormBuilder, 
    private service: UserService,
    @Inject(MAT_DIALOG_DATA) public data: User,
    public dialogRef: MatDialogRef<CreateUserComponent>) { }

  ngOnInit(): void {
    console.log(this.data['user']);
    this.user = this.data['user'];
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      name: [this.data['user']?.name, Validators.required],
      email: [this.data['user']?.email, Validators.required],
      password: [this.data['user']?.password, Validators.required],
    });
  }

  submit() {
    this.clicked = true;
    if(this.user == null) {
      this.create();
    } else {
      this.update();
    }
  }

  create() {
    this.service.createUser(this.form.value)
        .subscribe((result) => {
          console.log(result);
        });
  }

  update() {
    this.form.value.id = this.user.id;
    this.service.updateUser(this.form.value)
        .subscribe((result) => {
          console.log(result);
          this.closeModal(result);
        });
  }

  closeModal(event: any = null) {
    this.dialogRef.close(event);
  }

}
